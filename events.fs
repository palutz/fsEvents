type MyEventType() = 
  let event1 = new Event<_>()

type Worker(name : string, shift : string) =
   let mutable _name = name;
   let mutable _shift = shift;
   let nameChanged = new Event<unit>() (* creates event *)
   let shiftChanged = new Event<unit>() (* creates event *)

   member this.Name
      with get() = _name
      and set(value) = _name <- value

   member this.Shift
      with get() = _shift
      and set(value) = _shift <- value



type MyPerson(name : string) =
  let mutable _name = name;
  let nameChanged = new Event<_>()

  [<CLIEvent>]
  member this.NameChanged = 
            printfn "NameChanged event subscribed" 
            nameChanged.Publish

  member this.Name
    with get() = _name
    and set(value) = 
          _name <- value
          nameChanged.Trigger(this, value)


let bob = MyPerson("Bob")
bob.NameChanged.Add(fun (a, b) -> printfn "name changed")
bob.Name <- "Bob2"
