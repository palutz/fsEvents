type MyEventT () = 

  let sEvent = new Event<_> () 

  // member this.MEvent = printfn "%s" sEvent.Publish
  // member this.MEvent2 = printfn "Events2: %s" sEvent.Publish 

  member this.AddHandlers () = 
    Event.add (fun str -> str |> printfn "%s") sEvent.Publish
    Event.add (fun str -> str |> printfn "Second handler %s") sEvent.Publish


  member this.AddH f = 
    Event.add f sEvent.Publish

  member this.Trigger(msg) =
    sEvent.Trigger(msg)


let a = new MyEventT ()
a.AddHandlers ()
a.AddH (fun _ -> printfn "this is my handler")

a.Trigger("Ciao")

type eArg = {
  x : int
  y : int
}

let a = { x = 10; y = 11}

type MyEvent () = 
  let aEvent = new Event <_> ()

  [<CLIEvent>]
  member this.AEvent = aEvent.Publish

  member this.TrigIt (args: eArg) = 
    aEvent.Trigger (this, args)


let incr x = x + 1

let amv = MyEvent () 
amv.TrigIt a 

let aaa (a : eArg) = { a with (a.x |> incr) }

amv.AEvent.Add ( fun (e, arg) -> { arg with arg.x |> incr } ) 

type ATEvent (fp: 'a -> unit) = 
  let fpp = fp
  let atEvent = new Event <_> ()

  [<CLIEvent>]
  member this.ATEvent = atEvent.Publish

  member this.TrigIt (args: eArg) = 
        args |> fpp
        atEvent.Trigger args


let t = new ATEvent(printfn "triggering %A")
t.ATEvent.Add(fun arg -> { let bb = { arg with x = arg.x + 10 }; printfn "obj = %A res = %A" arg bb)}
t.ATEvent.Add(
      fun arg -> 
            let bb = { arg with x = arg.x + 10 }
            printfn "obj = %A res = %A" arg bb
      )

t.TrigIt { x = 10; y = 10 }

// inline helps to keep it generic, in this case it will require number to be convertible to float
// val inline printAsFloatingPoint :
//   number: ^a -> unit when  ^a : (static member op_Explicit :  ^a -> float)
let inline printAsFloatingPoint (number : 'a ) =
      printfn "%f" (float number)
